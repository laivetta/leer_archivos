# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import pathlib

import pandas as pd

import PyPDF2
import collections

# import os

# direct = "C:\\Users\\Majo\\borrar\\"


# lista_directorios= []
# lista_archivos= []


# def files(ruta):
#        for file in os.listdir(ruta):
           
#            if os.path.isfile(os.path.join(ruta, file)):
#                lista_archivos.append(file)
           
#            if os.path.isdir(os.path.join (ruta, file)):
#                lista_directorios.append(file)
               
#        print (lista_archivos)
#        print (lista_directorios)

# # files(direct)


#----------

direct = "C:\\Users\\Majo\\borrar\\"
dir_procesado = os.path.join(direct, "procesado")
dir_no_procesado = os.path.join (direct, "no_procesado")

directorios = {} 



def estructura (ruta):
    

    for path, dirs, files in os.walk(ruta):
        
        for d in dirs:
            dname = os.path.join(path, d)
            directorios [dname] = ""
          
            archivos = []
            for file in os.listdir(dname):
                f = file
                file = os.path.join(dname, file)
                if os.path.isfile(file):
                    archivos.append(file)
        
            directorios [dname] = archivos
    


def crear_directorio (a_crear = "encontrado"):
    """ voy a crear directorios donde mover lo que SI/NO se pudo leer
    
    """
    try:
        os.makedirs(os.path.join(direct, a_crear))
    
    except:
        print ("el directorio ya existe")



def mover_archivo (desde, hasta):   
        """ voy a mover lo que SI se pudo leer
    
        """
        import shutil

        try:
            shutil.move(desde, hasta)
    
        except:
            print ("no se pudo mover el archivo")
        




estructura(direct)
crear_directorio(dir_procesado)


#-------------
from zipfile import ZipFile 

def leer_zip ():
    try:
        for x,y in directorios.items():
            for i in y:
                if i.endswith("zip"):
                    # zip_path = os.path.join (name, y)
                    with ZipFile(i, 'r') as zip:
                        zip.printdir()
                        zip.extractall(os.path.dirname(i))
                    # os.remove(i)
            
    except:
        print ("Algun error en el arcivo ZIP")
        
# leer_zip()


#-------------------
pdf = {}


def leer_pdf():
    """esta funcion va a leer todo el texto que encuentre en los documentos 
    PDF hay que asignarla a una variable para poder trabajarla
    x es la ruta completa y nombre de archivo que voy a procesar"""
    try:
        for x,y in directorios.items():
            for i in y:
                if i.endswith("pdf"):
                    # zip_path = os.path.join (name, y)
                    pdfReader = PyPDF2.PdfFileReader(i)
                    num_page_pdf = pdfReader.numPages
                    
                    # for c in collections.Counter(range(num_page_pdf)):
                    #     pagina = pdfReader.getPage(c)
                    #     contenido_pagina = pagina.extractText()
                    #     # print(contenido_pagina.encode("utf-8"))
                    #     print (num_page_pdf)
                    
                    if num_page_pdf >= 0:
                        
                        # pathlib.Path(i).parts
                        #esto me muestra las partes de los directorios
                        # y yo los puedo recorrer
                        
                        if os.path.join(os.path.join(dir_procesado, pathlib.Path(i).parts[-2])) == True:
                            mover_archivo(i, os.path.join(os.path.join(dir_procesado, pathlib.Path(i).parts[-2]),os.path.basename(i)))
                            # es muy larga la linea pero es porque uso muchos joins para 
                            #poder armar toda la linea de directorio para mover
                            #el archivo encontrado
                        
                        else:
                            crear_directorio(os.path.join(os.path.join(dir_procesado, pathlib.Path(i).parts[-2])))
                        
                            mover_archivo(i, os.path.join(os.path.join(dir_procesado, pathlib.Path(i).parts[-2]),os.path.basename(i)))
                            # es muy larga la linea pero es porque uso muchos joins para 
                            #poder armar toda la linea de directorio para mover
                            #el archivo encontrado
                        
            
    except:
        print ("Algun error en el arcivo PDF")

leer_pdf()


# prueba = {"luis": {"hojas_pdf":1, "hojas_doc":3},
#           "pedro": {"hojas_pdf":2},
#           "juan": {"hojas_pdf":4, "hojas_doc": 4}
#           }

# df = pd.DataFrame(prueba).T




